--  Miniwelt Universitaet, eingeschraenkte Auswahl
--  entnommen aus Kemper/Eickler, 7. Auflage
--  Schema erweitert um Attribut Professoren (Gehalt)
--  und einige Constraints (O. Schäfer, 27.01.2015).

CREATE TABLE Studenten (
  MatrNr INTEGER
    NOT NULL CHECK (MatrNr BETWEEN 10000 AND 99999),
  Name VARCHAR (50)
    NOT NULL,
  Semester INTEGER
    NOT NULL CHECK (Semester > 0 AND Semester < 20) DEFAULT 1,
  CONSTRAINT stud_key PRIMARY KEY (MatrNr)
);
COMMENT ON Table Studenten IS 'Universitätsbeispiel';

CREATE TABLE Professoren
(
  PersNr INTEGER
    NOT NULL,
  Name VARCHAR (50)
    NOT NULL,
  Rang CHAR(2)
    CHECK (Rang IN ('C2', 'C3', 'C4')),
  Gehalt DECIMAL (10,2)
    CHECK (Gehalt BETWEEN 1000 AND 10000),
  Raum INTEGER,
  CONSTRAINT prof_key PRIMARY KEY (PersNr)
);
COMMENT ON Table Professoren IS 'Universitätsbeispiel';

CREATE TABLE Assistenten
(
  PersNr INTEGER
    NOT NULL,
  Name VARCHAR (50)
    NOT NULL,
  Fachgebiet VARCHAR (50),
  Boss INTEGER REFERENCES Professoren (PersNr)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT ass_key PRIMARY KEY (PersNr)
);
COMMENT ON Table Assistenten IS 'Universitätsbeispiel';

CREATE TABLE Vorlesungen
(
  VorlNr INTEGER
    NOT NULL,
  Titel VARCHAR (50)
    NOT NULL,
  SWS INTEGER
    CHECK (SWS > 0),
  gelesenVon INTEGER REFERENCES Professoren (PersNr)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT vorl_key PRIMARY KEY (VorlNr)
);
COMMENT ON Table Vorlesungen IS 'Universitätsbeispiel';

CREATE TABLE hoeren
(
  MatrNr INTEGER REFERENCES Studenten (MatrNr)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  VorlNr INTEGER REFERENCES Vorlesungen (VorlNr)
    ON UPDATE CASCADE,
  CONSTRAINT hoeren_key PRIMARY KEY (MatrNr, VorlNr)
);
COMMENT ON Table hoeren IS 'Universitätsbeispiel';

CREATE TABLE voraussetzen
(
  Vorgaenger INTEGER REFERENCES Vorlesungen (VorlNr)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  Nachfolger INTEGER REFERENCES Vorlesungen (VorlNr)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT voraussetzen_check
    CHECK (Vorgaenger < Nachfolger)
);
COMMENT ON Table voraussetzen IS 'Universitätsbeispiel';

CREATE TABLE pruefen
(
  MatrNr INTEGER REFERENCES Studenten
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  VorlNr INTEGER REFERENCES Vorlesungen
    ON UPDATE CASCADE,
  PersNr INTEGER REFERENCES Professoren
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  Note       NUMERIC (2,1)
    CHECK (Note BETWEEN 0.7 AND 5.0),
  CONSTRAINT pruefen_key    PRIMARY KEY (MatrNr, VorlNr)
);
COMMENT ON Table pruefen IS 'Universitätsbeispiel';
