--  Miniwelt Universitaet, eingeschraenkte Auswahl
--  entnommen aus Kemper/Eickler, 7. Auflage

DROP TABLE Studenten      CASCADE;
DROP TABLE Professoren    CASCADE;
DROP TABLE Assistenten    CASCADE;
DROP TABLE Vorlesungen    CASCADE;
DROP TABLE hoeren         CASCADE;
DROP TABLE voraussetzen   CASCADE;
DROP TABLE pruefen        CASCADE;
