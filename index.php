<!DOCTYPE html>
<html lang="de">
<link rel="stylesheet" type="text/css" href="lwbstyle.css" />
  <body>
  <h1> Ausgabe aller Prüfungsergebnisse der Studenten </h1>
  <?php 
    try {
      $con = new PDO ('pgsql:host=localhost;dbname=lewein', 'lewein', 'niewel');
      $vorlesungen = $con->query("SELECT s.name as student, s.semester, v.titel, p.name as professor, note
                                  FROM   (studenten s natural join pruefen natural join vorlesungen v), professoren p
                                  WHERE  gelesenVon = p.persnr");
      echo "<table><tr><th>Student</th><th>Semester</th><th>Titel</th><th>Professor</th><th>Note</th></tr>";
      foreach ($vorlesungen as $row) {
        echo "<tr>" .
               "<td>" . $row['student']   . "</td>" .
               "<td>" . $row['semester']  . "</td>" .
               "<td>" . $row['titel']     . "</td>" .
               "<td>" . $row['professor'] . "</td>" .
               "<td>" . $row['note']      . "</td>" .
             "</tr>";
      }
      echo "</table>";
      $con = null;
    } catch (PDOException $e) {
      echo "Fehler: " . htmlspecialchars ($e->getMessage ());
    }
  ?>
  </body>
</html>
